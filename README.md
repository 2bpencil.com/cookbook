# cookbook

A LaTeX class for typesetting a cookbook. It is based on the [cuisine](https://ctan.org/pkg/cuisine) package.